---
date: 2019-07-26
title: Git - Tutorial
categories:
  - Git
description: "Tutorial to manipulate the basics of Git"
type: Document
set: hmee325
set_order: 11
highlighter: rouge
---

## Objective

The objective of this tutorial is to familiarize yourself with the basics of Git so that you can quickly start using it.

The first exercise will be about creating a repository from scratch, adding some files, making some modifications and publishing it online.
This is what you will need when starting a new project.

The second one will make you learn how to start from an already existing repository and to provide some modifications to it.
This is more related to collaborative working.

## Gitlab

Before doing anything, please make sure that you have an account on [gitlab.com](https://gitlab.com) in order to host and share your Git repositories and access the ones that will be provided to you.

Once your account is created, it is recommended that you create an SSH key (if you don't already have one) and configure your Gitlab account with it.
This will allow you to work with your remote repositories without having to login at each access.
You can follow the official [Gitlab instructions](https://gitlab.com/help/ssh/README) if you don't know how to do this.

You can explore the repositories available for this module [here](https://gitlab.com/umrob/hmee325).

## Identify yourself

Before any commit can be made, you have to give some information about you to Git so that you can properly be identified in your commits.
Let's do this right now so that we don't have to think about it anymore.

From a terminal, just type the following commands with your real name and email address:
```bash
git config --global user.name "John Doe"
git config --global user.email "john@doe.com"
```

# Exercise #1
## Create a new repository

Let's start by creating a new empty repository.


From a terminal, create a new folder called *git-tutorial* and enter it:
```bash
mkdir git-tutorial
cd git-tutorial
```

Once this is done, initialize it using the `init` command :
```bash
git init 
```

To make sure everything went well, you can use the `status` command, which should give you something like:
```
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```

## Add new files

Now that the repository is created, we can add files to it.

We will start with a file called *README.md*.
Input the following command in your terminal to create this file with some text inside:
```bash
echo "Hello, I'm John Doe" > README.md
```

Let's now run the `status` command again to see what happened:
```
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	README.md

nothing added to commit but untracked files present (use "git add" to track)
```

We can see that the *README.md* is part of the *untracked* files.
This makes sense since we just created it and haven't told Git to track it yet.
To do so, we can do what is suggested and use the `add` command.
```bash
git add README.md
```

Now the `status` command should output:
```
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   README.md
```

Meaning that *README.md* file is now in the *staged* state, ready to be committed.

To create a new commit, the `commit` command can be used in two different ways:
```bash
git commit -m "my commit message"
# OR
git commit
```
The first one allows you to specify your commit description directly and is more appropriate for short messages.
The second one, on the other hand, will open up a text editor with a reminder of what has changed and where you have to write your commit message and is preferred for longer messages (e.g with multiple paragraphs).

Please keep in mind to **always** write **meaningful** commit messages.
Just writing *update* will make your repository's history harder to read and navigate.

Here, we can go with:
```bash
git commit -m "Adding a readme file to the project" -m "This is a Markdown file. If you need a refresher about Markdown, please visit https://www.markdownguide.org"
```

You can notice the use of multiple `-m` arguments in the `commit` command.
The first one indicates the title of the commit and the following one is used to add a new paragraph to the message.
This can be used to add any number of paragraphs.
The only mandatory information is the title but it is a good practice to add one or more paragraphs giving more details about the changes made.

If you run the `status` command, you should notice that the repository is now in a clean state:
```
On branch master
nothing to commit, working tree clean
```

To get the list of the commits, you can use the `log` command, which should show you something like (press *q* to quit):
```
commit bb85864f9ca18a06a681f4cb6e4ed1db58a95274 (HEAD -> master) 
Author: Benjamin Navarro <navarro.benjamin13@gmail.com> 
Date:   Mon Sep 2 18:26:42 2019 +0200 

    Adding a readme file to the project
    
    This is a Markdown file. If you need a refresher about Markdown, please visit https://www.markdownguide.org
```

Now that we are done with our new file, let's push it to an online repository.

## Pushing your modifications

Before we can do anything, we must create an online repository to host our project.

To do so, go to [gitlab.com](https://gitlab.com/), log in if necessary, and hit the *New project* button.
On the next page, you can enter the name of your project (it's better to keep the same name as the folder containing it) and a description.
You can also select if you want your project to be public or private.
Private repositories require special access rules for people other than you to access it.
Here select public and hit *Create project*.

Since we already have a local Git repository, we just need to configure its remote and push its current state.
You can find your remote repository's URL by selecting to *Clone > Clone with SSH* on your Gitlab project's home page.
```bash
git remote add origin git@gitlab.com:umrob/hmee325/git-tutorial.git
git push origin master
```
The first line registers a new remote called *origin* using the address *git@gitlab.com:umrob/hmee325/git-tutorial.git*.
The second one pushes the local state to the remote repository.

Once this is done, you can refresh your project's page to see its new content.

## Pulling modifications made by others

To simulate a modification performed by someone else on your remote repository, we will edit the *README.md* file online.
To do so, on your project's home page, click on the *README.md* file and then on the *Edit* button.
From here, you can change the text to something like *Hello, I'm a roboticist!*, give an appropriate commit message and hit the *Commit changes* button.

Now it's time to pull these modifications in order to update your local repository:
```bash
git pull origin master 
```
which you should give you an output similar to:
```
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From gitlab.com:umrob/hmee325/git-tutorial
 * branch            master     -> FETCH_HEAD
   bb85864..870a336  master     -> origin/master
Updating bb85864..870a336
Fast-forward
 README.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
```
giving your details about what has been updated.

Let's check that our local *README.md* file has now the updated content:
```bash
cat README.md 
Hello, I'm a roboticist!
```

Hooray, our local repository is now synchronized with out remote one!

## What we learned

In this first exercise, we saw how to:
 1. Create a new local Git project from scratch
 2. Add files to the project
 3. Create a commit to save the modifications
 4. Create a remote repository and push the local content to it
 5. Update the local repository with the modifications made on the remote one

In the next exercise, we will see how to start from an already existing repository, making changes and provide these changes to the original project.